import { createRouter, createWebHistory } from 'vue-router'
import Main from '@/pages/Main.vue';
import Authorisation from '@/pages/Authorisation.vue';
import Tasks from '@/pages/Tasks.vue';
import guard from '@/plugins/routerGuard.js';
import Users from '@/pages/Users.vue';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main,
      meta: {
        authOf: ['default', 'admin']
      }
    },
    {
      path: '/authorisation',
      name: 'Authorisation',
      component: Authorisation,
    },
    {
      path: '/users',
      name: 'Users',
      component: Users,
      meta: {
        authOf: ['admin']
      }
    },
    {
      path: '/tasks',
      name: 'Tasks',
      component: Tasks,
      meta: {
        authOf: ['default', 'admin']
      }
    },
  ]
});

router.beforeEach(guard);

export default router
