import { useUserStore } from '@/stores/userStore.js';

async function userLoad(next, store) {
  if (store.userIsLoaded) { return true; }
  try {
    await store.fetchUserData();
  } catch (error) {
    return false;
  }
  return true;
}

async function guard(to, from, next) {
  const store = useUserStore();
  if (!to.name) {
    next({ name: 'Tasks' });
    return;
  }
  if (to.meta.authOf === undefined) {
    next();
    return;
  }
  const loaded = await userLoad(next, store);
  const isAllowed = to.meta.authOf.includes(store.user?.role);
  if (isAllowed) {
    next();
  } else if (loaded) {
    next({ name: 'Main' }); // если не хватает ролей
  } else {
    next({ name: 'Authorisation' }); // если не авторизован
  }
}

export default guard;