import { defineStore } from 'pinia'
import api from '@/plugins/api.js';

export const useUserStore = defineStore({
  id: 'user',
  state: () => ({
    user: null
  }),
  actions: {
    async fetchUserData() {
      const { data } = await api.post('api/auth/me')
      this.user = data
      return true;
    },
    logout() {
      this.user = null
      localStorage.clear()
    },
  },
  getters: {
    userIsLoaded(state) {
      return !!state.user?.id
    },
  }
});